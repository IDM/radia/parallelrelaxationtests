// -------------------------------------------
// Preliminary tests for OpenMP and GPU acceleration of Radia
//
// This program mimicks the relaxation process used in Radia, using the following functions defined in radrlmet.cpp
//		void radTRelaxationMethNo_4::DefineNewMagnetizations()  |	Defines magnetizations at iteration k
//		void radTRelaxationMethNo_4::AutoRelax					|	Iterates magnetization computations
// 
// The code does the following:
// for n < n_iterations:
//		Compute new magnetizations
//		magnetizations = new_magnetization
//
// IT CANNOT BE COPY-PASTE TO RADIA BECAUSE A LOT OF SIMPLIFICATIONS WERE DONE. 
// THE MAIN PURPOSE WAS TO EVALUATE THE ACCELERATION WITH OPENMP AND OPEN CL AND TO TRY TO USE IT.
// I DID NOT TRIED TO OPTIMIZE THE USE OF THE GPU MEMORY. THERE WAS NO TRIAL TO USE THE MATRIX SYMMETRIES.
//
// NOTE: The OpenCL code is in the file relax.c
//
// The interaction matrix is modelled as follows:
//
//			(mat_00, mat_01,	...)
//	mat =	(...				...)	
//			(...,		mat_n-1 n-1)
//
// where the mat_ij are 3 x 3 matrices stored as a 9 elements 1D array, i.e. mat is a 3n x 3n 1D array
//
// The magnetizations and new magnetizations are vectors defined as 
//
//	mag = (mag_0, mag_1, ..., mag_n-1) with mag_i = (mag_i0, ..., mag_i2), i.e. mag is a 3n vector
//		and the same for new_mag
//
//
// G Le Bec, ESRF, May 2020
// -------------------------------------------

#include <chrono>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>

#include <cmath>

#include <omp.h> 

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp> 

using namespace std;
using namespace cl;

// --- Serial computations, reference
// Computation of new magnetizations
int define_new_mag_serial(int n_elem, double *mat, double *mag, double *new_mag) {
	
	double mat3[3][3];
	double vect3[3];
	int index, new_index;

	for (int i = 0; i < n_elem; i++) {
		new_index = 3 * i;
		new_mag[new_index++] = 0; new_mag[new_index++] = 0; new_mag[new_index] = 0;
		for (int j = 0; j < n_elem; j++) {
			// Get the matrix elements
			index = 9 * i + 9 * j * n_elem;
			mat3[0][0] = mat[index++]; mat3[0][1] = mat[index++]; mat3[0][2] = mat[index++];
			mat3[1][0] = mat[index++]; mat3[1][1] = mat[index++]; mat3[1][2] = mat[index++];
			mat3[2][0] = mat[index++]; mat3[2][1] = mat[index++]; mat3[2][2] = mat[index];
			// Get the vector elements
			index = 3 * j;
			vect3[0] = mag[index++]; vect3[1] = mag[index++]; vect3[2] = mag[index--];
			// Multiply
			new_mag[--index] += mat3[0][0] * vect3[0] + mat3[0][1] * vect3[1] + mat3[0][2] * vect3[2];
			new_mag[++index] += mat3[1][0] * vect3[0] + mat3[1][1] * vect3[1] + mat3[1][2] * vect3[2];
			new_mag[++index] += mat3[2][0] * vect3[0] + mat3[2][1] * vect3[1] + mat3[2][2] * vect3[2];
		}
	}
	return 0;
};
// Iterate the computation of the magnetizations (relaxation)
int test_serial(int n_iter, int n_elem, double *mat, double *mag, double *new_mag) {
	double * mag_tmp;
	for (int k = 0; k < n_iter; k++) {
		define_new_mag_serial(n_elem, mat, mag, new_mag);
		mag_tmp = mag;
		mag = new_mag;
		new_mag = mag_tmp; // To be overwritten at next iteration
	}
	return 0;
};

// --- Open MP: Multiprocessor and shared memory
// Computation of new magnetizations
int define_new_mag_openmp(int n_elem, double *mat, double *mag, double *new_mag, int n_th) {

	#pragma omp parallel num_threads(n_th)
	for (int i = 0; i < n_elem; i++) {
		int new_index = 3 * i;
		new_mag[new_index] = 0; new_mag[new_index + 1] = 0; new_mag[new_index + 2] = 0;
		// The inner loop is parallelized
		#pragma omp for
		for (int j = 0; j < n_elem; j++) {
			double mat3[3][3];
			double vect3[3];
			int index;
			// Get the matrix elements
			index = 9 * i + 9 * j * n_elem;
			mat3[0][0] = mat[index++]; mat3[0][1] = mat[index++]; mat3[0][2] = mat[index++];
			mat3[1][0] = mat[index++]; mat3[1][1] = mat[index++]; mat3[1][2] = mat[index++];
			mat3[2][0] = mat[index++]; mat3[2][1] = mat[index++]; mat3[2][2] = mat[index];
			// Get the vector elements
			index = 3 * j;
			vect3[0] = mag[index++]; vect3[1] = mag[index++]; vect3[2] = mag[index--];
			// Multiply
			new_mag[--index] += mat3[0][0] * vect3[0] + mat3[0][1] * vect3[1] + mat3[0][2] * vect3[2];
			new_mag[++index] += mat3[1][0] * vect3[0] + mat3[1][1] * vect3[1] + mat3[1][2] * vect3[2];
			new_mag[++index] += mat3[2][0] * vect3[0] + mat3[2][1] * vect3[1] + mat3[2][2] * vect3[2];
		}
	}
	return 0;
};
// Iterate the computation of the magnetizations (relaxation)
int test_openmp(int n_iter, int n_elem, double *mat, double *mag, double *new_mag) {

	double * mag_tmp;
	// Set the maximum number of parallel threads
	int n_th = omp_get_max_threads();
	cout << "Number of processors: " << n_th << endl;

	for (int k = 0; k < n_iter; k++) {
		define_new_mag_openmp(n_elem, mat, mag, new_mag, n_th);
		mag_tmp = mag;
		mag = new_mag;
		new_mag = mag_tmp; // To be overwritten at next iteration
	}
	return 0;
};

// --- OpenCL : GPU, parallel CPU, etc.
// Read OpenCL kernel code from file
int read_opencl_sourcecode(const char * filename, string & sourcecode) {
	
	fstream f;
	f.open(filename, fstream::in);
	if (!f) {
		std::cerr << "Error: File not found.\n";
		return 1;
	}

	char line[256];
	while (!f.eof()) {
		f.getline(line, 256);
		sourcecode.append(line);
		sourcecode.append("\n");
	}
	return 0;

}
// Print openCL device info
int print_opencl_devinfo(Device dev) {
	cout << "\tDevice Name: " << dev.getInfo<CL_DEVICE_NAME>() << endl;
	cout << "\tDevice Type: " << dev.getInfo<CL_DEVICE_TYPE>();
	cout << " (GPU: " << CL_DEVICE_TYPE_GPU << ", CPU: " << CL_DEVICE_TYPE_CPU << ")" << endl;
	cout << "\tDevice Vendor: " << dev.getInfo<CL_DEVICE_VENDOR>() << endl;
	cout << "\tDevice Max Compute Units: " << dev.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << endl;
	cout << "\tDevice Max work goupe size: " << dev.getInfo < CL_DEVICE_MAX_WORK_GROUP_SIZE>() << endl;
	cout << "\tDevice Global Memory: " << dev.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() << endl;
	cout << "\tDevice Max Clock Frequency: " << dev.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << endl;
	cout << "\tDevice Max Allocateable Memory: " << dev.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>() << endl;
	cout << "\tDevice Local Memory: " << dev.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() << endl;
	return 0;
}
// OpenCL host 
int test_opencl(int n_elem, int n_iter, double * mat, double * mag, double * new_mag) {

	// Get list of OpenCL platforms.
	vector<Platform> platform;
	Platform::get(&platform);
	if (platform.empty()) { cerr << "OpenCL platforms not found." << endl; return 1; }

	// Get the list of devices and print info
	Context context;

	vector<Device> devices;
	vector<std::size_t> wkgp_size;
	for (Platform p : platform) {
		cout << "Platform name: " << p.getInfo<CL_PLATFORM_NAME>() << endl;
		p.getDevices(CL_DEVICE_TYPE_GPU, &devices);
		for (Device dev : devices){
			print_opencl_devinfo(dev);
			wkgp_size.push_back(dev.getInfo < CL_DEVICE_MAX_WORK_GROUP_SIZE>());
		}
	}
	if (devices.empty()) { cerr << "Device not found" << endl; return 1; }
	context = Context(devices);

	// Create command queue
	CommandQueue queue(context, devices[0]);

	string source_str;
	char filename[] = "C:\\Users\\lebec\\Work Folders\\source\\repos\\ParallelRelaxationTests\\ParallelRelaxTests\\relax.c";
	read_opencl_sourcecode(filename, source_str);
	const char * opencl_source = source_str.c_str();

	// Compile OpenCL program
	Program program(context, Program::Sources(1, make_pair(opencl_source, strlen(opencl_source))));
	try { 
		program.build(devices); }
	catch (const Error&) {
		cerr << "OpenCL compilation error" << endl << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices[0]) << endl;
		return 1; }
	
	// OpenCL kernel 
	Kernel relax_iter_gpu(program, "relax_iter");

	// Allocate device buffers and transfer input data to device.
	cout << "Matrix size: " << 9 * n_elem * n_elem * sizeof(double) << endl;
	cout << "Vector size: " << 3 * n_elem * sizeof(double) << endl;
	
	Buffer buffer_mat(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 9 * n_elem * n_elem * sizeof(double), mat);
	Buffer buffer_mag(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 3 * n_elem * sizeof(double), mag);
	Buffer buffer_new_mag(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 3 * n_elem * sizeof(double), new_mag);

	
	// Set kernel parameters.
	relax_iter_gpu.setArg(0, static_cast<cl_ulong>(n_elem));
	relax_iter_gpu.setArg(1, static_cast<cl_ulong>(wkgp_size[0]));
	relax_iter_gpu.setArg(2, buffer_mat);
	relax_iter_gpu.setArg(3, buffer_mag);
	relax_iter_gpu.setArg(4, buffer_new_mag);


	// Launch kernels on the device.
	int n_iter_2 = n_iter / 2 ;
	cl::Event event;
	for (int k = 0; k < n_iter_2; k++) {
		// New magnetizations
		queue.enqueueNDRangeKernel(relax_iter_gpu, NullRange, wkgp_size[0], NullRange, NULL, &event);
		event.wait();
		// Swap the mag and new mag pointeurs
		relax_iter_gpu.setArg(3, buffer_new_mag);
		relax_iter_gpu.setArg(4, buffer_mag);
		queue.enqueueNDRangeKernel(relax_iter_gpu, NullRange, wkgp_size[0], NullRange, NULL, &event);
	}	

	// Get result back to host.
	queue.enqueueReadBuffer(buffer_new_mag, CL_TRUE, 0, 3 * n_elem * sizeof(double), new_mag);

	return 0;
};

int main()
{
	int num_radia_elem = 4000; // Number of elements
	int num_iter = 200; // Number of iterations
	cout << "Number of elements:   " << num_radia_elem << endl;
	cout << "Number of iterations: " << num_iter << endl;
	cout << "Initialization..." << endl;
	chrono::time_point<chrono::system_clock> t0, t1, t2, t3, t4, t5, t6;

	// --- Initialize fake relaxation matrix and magnetization array
	t0 = chrono::system_clock::now();

	double *p_iter_mat = nullptr, *p_mag = nullptr, *p_new_mag = nullptr;
	p_iter_mat = new double[9 * num_radia_elem * num_radia_elem];
	p_mag = new double[3 * num_radia_elem];
	p_new_mag = new double[3 * num_radia_elem];
	for (int i = 0; i < 3 * num_radia_elem; i++) {
		p_mag[i] = rand() / RAND_MAX - 0.5;
		p_new_mag[i] = 0;
	}
	for (int i = 0; i < 9 * num_radia_elem * num_radia_elem; i++) 
		p_iter_mat[i] = rand() / RAND_MAX - 0.5;

	// --- 1 / No parallelization at all
	cout << "1 / Serial" << endl;
	t1 = chrono::system_clock::now();
	test_serial(num_iter, num_radia_elem, p_iter_mat, p_mag, p_new_mag);
	t2 = chrono::system_clock::now();

	// --- 2 / OpenMP
	cout << "2 / With OpenMP" << endl;
	t3 = chrono::system_clock::now();
	test_openmp(num_iter, num_radia_elem, p_iter_mat, p_mag, p_new_mag);
	t4 = chrono::system_clock::now();

	// --- 3 / OpenCL
	cout << "3 / With OpenCL" << endl;
	t5 = chrono::system_clock::now();
	test_opencl(num_radia_elem, num_iter, p_iter_mat, p_mag, p_new_mag);
	t6 = chrono::system_clock::now();

	// --- Time count
	cout << "\nTime count" << endl;
	cout << "Vectors created in " << chrono::duration_cast<chrono::milliseconds>(t1 - t0).count() << " ms" << endl;
	cout << "Relaxation without parallelization in " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << " ms" << endl;
	cout << "Relaxation with OpenMP in " << chrono::duration_cast<chrono::milliseconds>(t4 - t3).count() << " ms" << endl;
	cout << "Relaxation with OpenCL in " << chrono::duration_cast<chrono::milliseconds>(t6 - t5).count() << " ms" << endl;

	// --- Clear
	delete[] p_iter_mat; p_iter_mat = nullptr;
	delete[] p_mag; p_mag = nullptr;
	delete[] p_new_mag; p_new_mag = nullptr;

}

