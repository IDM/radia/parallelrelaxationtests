// -------------------------------
// Main kernel function
// Compute new magnetizations
// -------------------------------
kernel void relax_iter(
	ulong n_elem,	// Number of elements
	ulong wkgp_size,	// Work group size
	global const double *mat, // Interaction matrix
	global double *mag, // Initial magnetizations
	global double *new_mag) // New magnetizations
{
	// Number of elements
	unsigned elem_per_kernel = n_elem / wkgp_size; // Per kernel
	unsigned elem_remaining = n_elem - elem_per_kernel * wkgp_size; //Remaining

	// Kernel id
	unsigned i = get_global_id(0);
	unsigned i_elem, i_mat, i_mag, i_mag_new;

	double mat3[3][3], vect3[3];

	for (unsigned j = 0; j < elem_per_kernel; j++) {
		i_elem = i + elem_per_kernel * j;
		i_mag_new = 3 * i_elem;
		new_mag[i_mag_new++] = 0; new_mag[i_mag_new++] = 0; new_mag[i_mag_new--] = 0; i_mag_new--;
		for (unsigned k = 0; k < n_elem - 1; k++) {
			// Get the matrix
			i_mat = 9 * i_elem + 9 * k * n_elem;
			mat3[0][0] = mat[i_mat++]; mat3[0][1] = mat[i_mat++]; mat3[0][2] = mat[i_mat++];
			mat3[1][0] = mat[i_mat++]; mat3[1][1] = mat[i_mat++]; mat3[1][2] = mat[i_mat++];
			mat3[2][0] = mat[i_mat++]; mat3[2][1] = mat[i_mat++]; mat3[2][2] = mat[i_mat];

			// Get the magnetization of elements
			i_mag = 3 * k;
			vect3[0] = mag[i_mag++]; vect3[1] = mag[i_mag++]; vect3[2] = mag[i_mag];

			// Multiply
			new_mag[i_mag_new++] += mat3[0][0] * vect3[0] + mat3[0][1] * vect3[1] + mat3[0][2] * vect3[2];
			new_mag[i_mag_new++] += mat3[1][0] * vect3[0] + mat3[1][1] * vect3[1] + mat3[1][2] * vect3[2];
			new_mag[i_mag_new--] += mat3[2][0] * vect3[0] + mat3[2][1] * vect3[1] + mat3[2][2] * vect3[2];
			i_mag_new--;
		}
	}
	// Remaining elements to process
	i_elem = elem_per_kernel * wkgp_size + i;
	i_mag_new = 3 * i_elem;
	new_mag[i_mag_new++] = 0; new_mag[i_mag_new++] = 0; new_mag[i_mag_new--] = 0; i_mag_new--;

	for (unsigned k = 0; k < n_elem - 1; k++) {
			
		// Get the matrix
		i_mat =  9 * i_elem + 9 * k * n_elem;
		mat3[0][0] = mat[i_mat++]; mat3[0][1] = mat[i_mat++]; mat3[0][2] = mat[i_mat++];
		mat3[1][0] = mat[i_mat++]; mat3[1][1] = mat[i_mat++]; mat3[1][2] = mat[i_mat++];
		mat3[2][0] = mat[i_mat++]; mat3[2][1] = mat[i_mat++]; mat3[2][2] = mat[i_mat];
			
		// Get the magnetization of elements
		i_mag = 3 * k;
		vect3[0] = mag[i_mag++]; vect3[1] = mag[i_mag++]; vect3[2] = mag[i_mag];
			
		// Multiply
		new_mag[i_mag_new++] = 0; // += mat3[0][0] * vect3[0] + mat3[0][1] * vect3[1] + mat3[0][2] * vect3[2];
		new_mag[i_mag_new++] = 0;// += mat3[1][0] * vect3[0] + mat3[1][1] * vect3[1] + mat3[1][2] * vect3[2];
		new_mag[i_mag_new--] = 0;// += mat3[2][0] * vect3[0] + mat3[2][1] * vect3[1] + mat3[2][2] * vect3[2];
		i_mag_new--;
	}
		

}

