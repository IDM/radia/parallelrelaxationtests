# ParallelRelaxationTests

## Introduction
Preliminary tests for OpenMP and GPU acceleration of Radia.

This program mimicks the relaxation process used in Radia, using the following functions defined in radrlmet.cpp
	void radTRelaxationMethNo_4::DefineNewMagnetizations()  |	Defines magnetizations at iteration k
	void radTRelaxationMethNo_4::AutoRelax					|	Iterates magnetization computations

The code does the following:
for n < n_iterations:
	Compute new magnetizations
	magnetizations = new_magnetization

THIS CODE IS NOT INTENDED TO BE COPY-PASTE TO RADIA BECAUSE A LOT OF SIMPLIFICATIONS WERE DONE HERE. 
HE MAIN PURPOSE WAS TO EVALUATE THE ACCELERATION WITH OPENMP AND OPEN CL AND TO TRY TO USE IT.
I DID NOT TRIED TO OPTIMIZE THE USE OF THE GPU MEMORY. THERE WAS NO TRIAL TO USE THE MATRIX SYMMETRIES.

NOTE: The OpenCL code is in the file relax.c
The path to this file is defined line 201. 
In the present implementation the file is read at run time. It is also possible to include as a char array in the .cpp file, but the impact on the computation time should be negligible.

The interaction matrix is modelled as follows:

			(mat_00 mat_01		...)
	mat =		(...			...)	
			(...		mat_n-1 n-1)

where the mat_ij are 3 x 3 matrices stored as a 9 elements 1D array, i.e. mat is a 3n x 3n 1D array

The magnetizations and new magnetizations are vectors defined as 

	mag = (mag_0, mag_1, ..., mag_n-1) with mag_i = (mag_i0, ..., mag_i2), i.e. mag is a 3n vector 

and the same for new_mag


## Preliminary results
The first tests were ran on a work-station equipped with a 8 core Intel Xeon CPU E5-1630 v3 and a NVIDIA Quadro K2200 graphic card.

With 4000 elements and 200 iteration, the typical computation times are 

Run		|	1 CPU	|	OpenMP	|	1 GPU
--------|-----------|-----------|---------			
1		|	83 s	| 	18.5 s 	|	6.7 s
2		|	82 s 	|	19.0 s 	|	6.8 s


## To do
- [ ] Check the accuracy of the computations. I am confident that the 1 CPU and the OpenMP methods give the same results. This is less obvious for the GPU method, to be properly tested.

- [ ] Optimize the use of the GPU memory. In the present version the data are stored in the global memory which is known to be slow.

- [ ] Try to adapt to the Radia relaxation